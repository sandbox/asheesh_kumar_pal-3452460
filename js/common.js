(function ($, Drupal, window, document, undefined) {
  Drupal.behaviors.common_js_1 = {
    attach: function (context, settings) {
      // Ensure code runs only once per element using core/once
      // Code for removing items from cart block
      once(
        "cart-summary",
        $("#block-vitoria-cart .cart-block--summary__count", context)
      ).forEach(function (element) {
        var text = $(element).text();
        var newText = text.replace("items", "");
        $(element).text(newText);
      });

      // Code for all categories dropdown
      once(
        "product-categories",
        $("#block-vitoria-productcategoriesmenu h2", context)
      ).forEach(function (element) {
        $(element).click(function () {
          $(".facets-widget-links", context).slideToggle(400);
        });
      });

      // Hide show all categories menu on Scroll
      var $blockFacets = $(".path-frontpage .block-facets");
      if ($blockFacets.length > 0) {
        var stickyHeader = $blockFacets.offset().top;
        $(window).on("scroll", function () {
          if ($(this).scrollTop() > stickyHeader) {
            $blockFacets.addClass("sticky");
          } else {
            $blockFacets.removeClass("sticky");
          }
        });
      }

      // mobile version header
      once("open-search-box", $("#searchIcon", context)).forEach(function (element) {
        $(element).click(function() {
            $("#block-vitoria-exposedformproduct-catalogpage-1").toggleClass("show");
            if ($("#block-vitoria-exposedformproduct-catalogpage-1").hasClass("show")) {
               
                $("#block-vitoria-exposedformproduct-catalogpage-1").focus();
    
                $(element).text("✖");
            } else {
                
                $(element).text("🔍");
            }
        });
    });
    
      
    
    

      //Quick tabs for product detail page
      if (!context.querySelector(".custom-tabs")) {
        return;
      }
      const tabs = context.querySelectorAll(".custom-tabs li");
      const tabContents = context.querySelectorAll(".tab-content");

      tabs.forEach((tab) => {
        tab.addEventListener("click", function () {
          // Remove active class from all tabs
          tabs.forEach((tab) => tab.classList.remove("active"));
          // Hide all tab contents
          tabContents.forEach((content) => content.classList.remove("show"));

          // Add active class to the clicked tab
          this.classList.add("active");
          // Show the corresponding tab content
          const tabId = this.getAttribute("data-set");
          context.querySelector(`.${tabId}`).classList.add("show");
        });
      });

      // jQuery-based handling with context awareness
      $(".primary-nav__menu--level-2", context).first().show();
      once("hamburger-menu", $(".hamburger-menu", context)).forEach(function (
        element
      ) {
        $(element).click(function () {
          $(".primary-nav__menu--level-2", context).first().slideToggle(400);
        });
      });

      // Burger menu handling
      once("burger-menu", $("#header", context)).forEach(function (
        headerElement
      ) {
        $("#burger", context)
          .off("click")
          .click(function () {
            $(".vitoria-menu", headerElement)
              .toggleClass("menu-opened")
              .slideToggle(500);
            $("body", context).toggleClass("overflow-hidden");
          });
      });
    },
  };
})(jQuery, Drupal, window, document);
